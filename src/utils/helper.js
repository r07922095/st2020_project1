
let sorting = (array) => {
    array.sort();
    return array;
}

let compare = (a, b) => {
  if (a['PM2.5'] < b['PM2.5'])
  {
    return -1;
  }
  if (b['PM2.5'] < a['PM2.5'])
  {
    return 1;
  }
  return 0;
}

let average = (nums) => {
  let sum = 0;
  for (var i = 0; i < nums.length; i++)
  {
    sum += nums[i]
  }
  let average = sum / nums.length;
  if (!(average % 1 === 0))
  {
    average = Math.round(average*100) / 100
  }
	   return average;
}


module.exports = {
    sorting,
    compare,
    average
}
